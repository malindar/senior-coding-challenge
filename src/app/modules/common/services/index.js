const GitHubService = require('./GithubService');
const TwitterService = require('./TwitterService');

module.exports = {
    GitHubService,
    TwitterService
};
