const axios = require('axios');
const config = require('../../../config');
const {host} = config.get('gitHubAPI');


const formatRepoDetails = (repos, count) => {
  const items = repos.slice(0,count)
   const formattedResults = items.map(repo => {
     const {id, name, full_name, owner: {login, type}} = repo;
     return {id, name, full_name, owner: {login, type}};
   });

   return formattedResults;
};

const listRepos = (page = 1, count) => {
  return new Promise((resolve, reject) => {
      axios.get(`${host}?q=reactive&page=${page}`, {}, { headers: { 'Content-Type': 'application/json' } })
      .then(response => {
          resolve(formatRepoDetails(response.data.items, count));
      })
      .catch(e => {
          reject(e);
      })
  });
};

module.exports = {
  listRepos,
};
