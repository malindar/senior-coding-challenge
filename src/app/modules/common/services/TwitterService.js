const Twit = require('twitter');
const config = require('../../../config');
const twitterConfig = config.get('twitter');
const T = new Twit(twitterConfig);

const formattedTweets = (tweets) => {
    return tweets.statuses.map(tweet => {
        const {created_at, id, text, user : {name}, entities: {hashtags}} = tweet;
        return {created_at, id, text, user: name, hashtags }
    })
};

const getTweetsByKeyWords = (keyWord, id) => {
    return new Promise((resolve, reject) => {
        try {
            T.get('search/tweets', {q: keyWord}, function(error, tweets, response) {
                if(error){
                    console.log(error);
                    resolve({});
                } else {
                    resolve({tweets: formattedTweets(tweets), gitId: id});
                }
            })
        } catch(e) {
            reject(e)
        } 
    });
}; 

module.exports = {
    getTweetsByKeyWords,
};

