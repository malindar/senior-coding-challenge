const Promise = require('bluebird');
const SearchService = require('../service');

module.exports.search = ({options}) => {
  const {params} = options;
  return new Promise(resolve => {
    SearchService
      .GitHubTwitterMash(params.page || 1)
      .then(rec => {
        return resolve({ code: 200, data: rec });
      })
      .catch(e => {
        return resolve({ code: 400, data: {}, message: e.message, error: e });
      });
  });
};

