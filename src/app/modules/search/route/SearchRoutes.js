const express = require("express");
const { searchCtrl } = require("../controller");
const router = express.Router();

function SearchRoutes(handler) {
  router.route("/").get(handler(searchCtrl.search));

  return router;
}
module.exports = SearchRoutes;
