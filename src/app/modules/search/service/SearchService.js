
const Promise = require('bluebird');
const _ = require('lodash');
const {TwitterService, GitHubService} = require('../../common/services');

const formatResults = (gitRepos, tweets) => {
    const tweetsByRepo = _.groupBy(tweets, 'gitId')
    return Promise.map(gitRepos, repo => {
        repo.tweets =  tweetsByRepo[repo.id][0].tweets
        return repo;
    })
    
};

const GitHubTwitterMash = (page) => {
    let repos = []
    return new Promise((resolve, reject) => {
        GitHubService.listRepos(page, 10)
        .then(gitRepos => {
            repos = gitRepos
            const tweets =  Promise.map(gitRepos, repo => {
                const {name, id} = repo;
                return TwitterService.getTweetsByKeyWords(name, id);
                
            })
            return tweets;
        })
        .then(tweets => {
            return formatResults(repos, tweets)
        })
        .then(gitTweet => {
            resolve(gitTweet);
        })
    });
};

module.exports = {
    GitHubTwitterMash
};
