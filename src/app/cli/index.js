#!/usr/bin/env node
const gitTweet = require('../modules/search/service');

const result = gitTweet.GitHubTwitterMash();
result.then(res => {
    console.log('=====================Table view=======================');
    console.table(res);
    console.log('=====================Table view end =======================')
    console.dir(res, { depth: null }); 
})
